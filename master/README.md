# Usage

1. Generate the Certificate with the `make` command and answer all the questions
2. Start Server with `cargo run`
3. Connect Browsers with **https**://${Your IP}

# Progress

- [x] Setup static Content
- [x] Setup Crypto
- [x] WebSocket connection (Server & Client)
- [x] Subscribe to Events as Client
- [x] Subscribe to Events as Pod
- [x] Publish Meta-Data
- [x] Update Meta-Data
- [x] Publish Pictures
- [x] Select Gallery
- [x] View Pictures

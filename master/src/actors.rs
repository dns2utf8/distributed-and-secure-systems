use ::actix::*;
//use ::actix_web::*;
//use actix_web::{web, App, Error, HttpRequest, HttpResponse, HttpServer};
use ::actix_web_actors::ws;

use ::chrono::{Utc, DateTime};

use crate::protocols::{PodId, PodRequest, PodResponse, ClientRequest, ClientRequestAsync, ClientResponse};

use std::collections::HashMap;

#[derive(Default)]
pub struct Hub {
    pods: HashMap<PodId, PodInfo>,
    clients: HashMap<PodId, Addr<Ws>>,
}

impl Hub {
    /*fn broadcast<T, M, R>(&self, message: M) where T: actix::Handler<M>, M: Message<Result=R> + Clone + Send, R: 'static + Send {
        for pod in self.pods.values() {
            pod.addr.do_send(message.clone())
        }
    }*/
    fn broadcast_client_response(&self, message: ClientResponse) {
        for addr in self.clients.values() {
            addr.do_send(message.clone())
        }
    }
    /*
    fn broadcast_pod_response(&self, message: PodResponse) {
        for pod in self.pods.values() {
            pod.addr.do_send(message.clone())
        }
    }*/
}

impl Actor for Hub {
    type Context = SyncContext<Self>;
}

impl Handler<SubscribeClient> for Hub {
    type Result = ();

    fn handle(&mut self, msg: SubscribeClient, _ctx: &mut Self::Context) -> Self::Result {
        self.clients.insert(msg.id, msg.addr);
        self.handle(ClientRequest::ListAllPods, _ctx);
    }
}
impl Handler<SubscribePod> for Hub {
    type Result = ();

    fn handle(&mut self, msg: SubscribePod, _ctx: &mut Self::Context) -> Self::Result {
        self.pods.insert(msg.id, PodInfo {
            addr: msg.addr,
            name: msg.name.clone(),
            image_paths: vec![],
            last_modified: Utc::now(),
        });
        self.broadcast_client_response(ClientResponse::NewPod { id: msg.id, name: msg.name, });
    }
}
/*
impl Handler<UnsubscribePod> for Hub {
    type Result = ();

    fn handle(&mut self, msg: UnsubscribePod, _ctx: &mut Self::Context) -> Self::Result {
        if let Some(_lost_pod) = self.pods.remove(&msg.0) {
            let message = ClientResponse::PodGone(msg.0);
            self.broadcast_client_response(message);
            println!("removing pod {}:{:?}", msg.0, _lost_pod.name);
        }
    }
}*/
impl Handler<UnsubscribeClient> for Hub {
    type Result = ();

    fn handle(&mut self, msg: UnsubscribeClient, _ctx: &mut Self::Context) -> Self::Result {
        self.clients.remove(&msg.0);

        // and pod if it was serving data
        if let Some(lost_pod) = self.pods.remove(&msg.0) {
            let message = ClientResponse::PodGone(msg.0);
            self.broadcast_client_response(message);
            println!("removing pod {}: {:?}", msg.0, lost_pod);
            println!("  > remaining: {:?}", self.pods);
        }

        println!("UnsubscribeClient: {:?}", msg.0);
    }
}
impl Handler<ClientRequest> for Hub {
    type Result = MessageResult<ClientRequest>;

    fn handle(&mut self, msg: ClientRequest, _ctx: &mut Self::Context) -> Self::Result {
        use ClientRequest::*;
        let r = match msg {
            ListAllPods => {
                let pods = self.pods.iter().map(|(&id, info)| {
                    crate::protocols::PodDescription {
                        id,
                        name: info.name.clone(),
                        paths: info.image_paths.clone(),
                        last_modified: info.last_modified,
                    }
                }).collect();
                ClientResponse::Pods(pods)
            }
            ListPodStructure(id) => {
                match self.pods.get(&id) {
                    Some(info) => {
                        ClientResponse::PodUpdatePaths {
                            id,
                            paths: info.image_paths.clone(),
                            replace_images: false,
                            last_modified: info.last_modified,
                        }
                    }
                    None => {
                        ClientResponse::UnknownPod(id)
                    }
                }
            }
        };
        MessageResult(r)
        //_ctx.repyl(r)
    }
}
impl Handler<ClientRequestAsync> for Hub {
    type Result = ();

    fn handle(&mut self, msg: ClientRequestAsync, _ctx: &mut Self::Context) -> Self::Result {
        use ClientRequestAsync::*;
        match msg {
            RequestImage { client_id, gallery_id, path } => {
                match self.pods.get(&gallery_id) {
                    Some(pod) => {
                        pod.addr
                            .do_send(PodResponse::RequestImage {
                                path, client_id,
                            })
                    }
                    None => {
                        if let Some(client) = self.clients.get(&client_id) {
                            client.do_send(
                                ClientResponse::UnknownPod(gallery_id)
                            )
                        }
                    }
                }
            }
        }
    }
}

impl Handler<IdedPodRequest> for Hub {
    type Result = ();

    fn handle(&mut self, msg: IdedPodRequest, _ctx: &mut Self::Context) -> Self::Result {
        use PodRequest::*;
        match msg.message {
            RegisterSelf { .. } => unreachable!("must be handled by Ws"),
            UpdateTitle { name } => {
                self.pods.get_mut(&msg.id).expect("unable to find PodInfo").name = name.clone();
                self.broadcast_client_response(ClientResponse::PodUpdateName{ id: msg.id, name, });
            }
            UpdatePaths { mut paths, replace_images } => {
                paths.sort();
                paths.dedup_by(|a, b| a == b);
                let now = Utc::now();
                let pod_info = self.pods.get_mut(&msg.id).expect("unable to find PodInfo");
                pod_info.image_paths = paths.clone();
                pod_info.last_modified = now;
                self.broadcast_client_response(ClientResponse::PodUpdatePaths{
                    id: msg.id,
                    paths,
                    replace_images,
                    last_modified: now,
                });
            }
            DeliverImage { client_id, path, blob } => {
                if let Some(client) = self.clients.get(&client_id) {
                    client.do_send(ClientResponse::DeliverImage {
                        gallery_id: msg.id,
                        path,
                        blob,
                    });
                }
            }
        }
    }
}

pub struct PodInfo {
    addr: Addr<Ws>,
    name: String,
    image_paths: Vec<String>,
    last_modified: DateTime<Utc>,
}
impl std::fmt::Debug for PodInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PodInfo")
            .field("name", &self.name)
            .field("image_paths", &self.image_paths)
            .finish()
    }
}


/// Define http actor
pub struct Ws {
    pub id: PodId,
    pub hub: Addr<Hub>,
    pub is_pod: bool,
}

/*
impl Ws {
    fn client_request(&mut self, msg: ClientRequest, ctx: &mut ws::WebsocketContext<Self>) {
        self.hub.send(msg)
            .into_actor(self)
            .then(|response, _, ctx| {
                self.do_send(response);
                fut::ok( () )
            })
            .spawn(ctx);
    }

    fn pod_request(&mut self, msg: PodRequest, ctx: &mut ws::WebsocketContext<Self>) {
        // match msg {}
    }
}
*/
impl Actor for Ws {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.hub.do_send(SubscribeClient {
            id: self.id,
            addr: ctx.address(),
        });
    }

    fn stopped(&mut self, _ctx: &mut Self::Context) {
        self.hub.do_send(UnsubscribeClient(self.id));
    }
}

/// Handler for ws::Message message
impl StreamHandler<ws::Message, ws::ProtocolError> for Ws {

    fn handle(&mut self, msg: ws::Message, ctx: &mut Self::Context) {
        match msg {
            ws::Message::Ping(msg) => ctx.pong(&msg),
            ws::Message::Text(text) => {
                use crate::protocols::JsonProtocol;
                let message: Result<JsonProtocol, _> = serde_json::from_str(&text);
                match message {
                    Ok(JsonProtocol::ClientRequest(message)) => {
                        self.hub.send(message)
                            .into_actor(self)
                            .then(|response, this, ctx| {
                                let response: ClientResponse = response.expect("error processing ClientRequest");
                                actix::Handler::handle(this, response, ctx);
                                fut::ok( () )
                            })
                            .spawn(ctx);
                    }
                    Ok(JsonProtocol::ClientRequestAsync(message)) => {
                        actix::Handler::handle(self, message, ctx);
                    }
                    Ok(JsonProtocol::PodRequest(message)) => {
                        //self.pod_request(message, ctx);
                        actix::Handler::handle(self, message, ctx);
                    }
                    _invalid => {
                        ctx.text(format!("{{\"invalid request\":{:?}}}", _invalid));
                    }
                }
            },
            ws::Message::Binary(bin) => ctx.binary(bin),
            ws::Message::Close(reason) => ctx.close(reason),
            _ => (),
        }
    }
}
impl Handler<ClientRequestAsync> for Ws {
    //type Result = MessageResult<()>;
    type Result = ();

    fn handle(&mut self, mut message: ClientRequestAsync, _ctx: &mut Self::Context) -> Self::Result {
        // Don't ever trust the client
        if let ClientRequestAsync::RequestImage { client_id, .. } = &mut message {
            *client_id = self.id;
        };
        self.hub.do_send(message);
    }
}
impl Handler<ClientResponse> for Ws {
    type Result = MessageResult<ClientResponse>;

    fn handle(&mut self, message: ClientResponse, ctx: &mut Self::Context) -> Self::Result {
        let message = crate::protocols::JsonProtocol::ClientResponse(message);
        ctx.text(serde_json::to_string(&message).expect("unable to serialize internal state"));
        MessageResult(())
    }
}
impl Handler<PodResponse> for Ws {
    type Result = MessageResult<PodResponse>;

    fn handle(&mut self, message: PodResponse, ctx: &mut Self::Context) -> Self::Result {
        let message = crate::protocols::JsonProtocol::PodResponse(message);
        ctx.text(serde_json::to_string(&message).expect("unable to serialize internal state"));
        MessageResult(())
    }
}

impl Handler<PodRequest> for Ws {
    type Result = MessageResult<PodRequest>;

    fn handle(&mut self, message: PodRequest, ctx: &mut Self::Context) -> Self::Result {
        use PodRequest::*;
        match message {
            RegisterSelf { name, .. } => {
                if self.is_pod == false {
                    self.is_pod = true;
                    self.hub.do_send(SubscribePod { id: self.id, name, addr: ctx.address(), });
                    actix::Handler::handle(self, PodResponse::Registered { global_id: self.id }, ctx);
                } else {
                    actix::Handler::handle(self, PodResponse::AlreadyRegistered { global_id: self.id }, ctx);
                }
            }
            other_messages => {
                self.hub.do_send(IdedPodRequest { id: self.id, message: other_messages });
            }
        };
        MessageResult(())
    }
}

#[derive(Message)]
pub struct SubscribePod {
    id: PodId,
    addr: Addr<Ws>,
    name: String,
}

#[derive(Message)]
pub struct UnsubscribePod(PodId);
#[derive(Message)]

pub struct SubscribeClient {
    id: PodId,
    addr: Addr<Ws>,
}

#[derive(Message)]
pub struct UnsubscribeClient(PodId);

#[derive(Message)]
pub struct IdedPodRequest {
    id: PodId,
    message: PodRequest,
}

#[macro_use]
extern crate actix;
extern crate actix_web;
//extern crate actix_net;
extern crate futures;

#[macro_use]
extern crate serde_derive;


use actix::*;
use actix_files as fs;
use actix_web::{web, App, HttpRequest, HttpServer};
use actix_web::web::Data;
use actix_web_actors::ws;

//use native_tls::{Identity, TlsAcceptor};
use rustls::{NoClientAuth, ServerConfig};

use std::fs::File;
use std::sync::{Arc, Mutex};

mod protocols;
use protocols::PodId;

mod actors;
use actors::{Hub, Ws};


fn main() {
    let bind_addr = "[::]:3000";
    std::env::set_var("RUST_BACKTRACE", "1");
    std::env::set_var("RUST_LOG", "actix_server=info,actix_web=trace");

    protocols::print_all_messages();

    env_logger::init();

    let sys = actix::System::new("master process");

    let rustls_acceptor = get_rustls_acceptor().expect("unable to initialize TLS");

    let incrementor = Arc::new(Mutex::new(
        Incrementor { i: 0 }
    ));

    // Start only one instance of our central Hub
    let hub = SyncArbiter::start(1, || {
        Hub::default()
    });

    println!("binding to https://{}", bind_addr);
    HttpServer::new(move || {
        App::new()
        .data((incrementor.clone(), hub.clone()))
        .service(web::resource("/ws/")
            .to(|req: HttpRequest, stream: web::Payload, data: Data<(Arc<Mutex<Incrementor>>, Addr<Hub>)>| {
                let (incrementor, hub) = &*data;
                ws::start(
                    Ws {
                        id: incrementor.lock().unwrap().increment(),
                        hub: hub.clone(),
                        is_pod: false,
                    },
                    &req,
                    stream
                )
        }))
        .service(fs::Files::new("/", "./static/").show_files_listing().index_file("index.html"))
        //.finish();
    })
    //.bind_tls(bind_addr, native_acceptor)
    .bind_rustls(bind_addr, rustls_acceptor)
    .expect("unable to construct HttpServer")
    .start();

    println!("sys.run()");
    let _ = sys.run();
}

/*
fn get_native_tls_acceptor() -> Result<TlsAcceptor, native_tls::Error> {
    const P12_PASSWORD: &str = "";

    let mut file = File::open("identity.pfx").expect("unable to open identity");
    let mut identity = vec![];
    file.read_to_end(&mut identity).expect("unable to read identity");
    let identity = Identity::from_pkcs12(&identity, P12_PASSWORD)?;

    TlsAcceptor::new(identity)
}*/

fn get_rustls_acceptor() -> Result<ServerConfig, rustls::TLSError> {
    use rustls::internal::pemfile::{certs, rsa_private_keys};
    use std::io::BufReader;

    let mut config = ServerConfig::new(NoClientAuth::new());

    let cert_file = &mut BufReader::new(File::open("cert.pem").expect("unable to read cert.pem"));
    let key_file = &mut BufReader::new(File::open("key_decrypted.pem").expect("unable to read key_decrypted.pem"));

    let cert_chain = certs(cert_file).expect("unable to construct certificate chain");

    let mut keys = rsa_private_keys(key_file).expect("unable to construct keys");

    config.set_single_cert(cert_chain, keys.pop().expect("no private keys found or unable to read, is it encrypted?"))?;

    Ok(config)
}

struct Incrementor {
    i: PodId,
}
impl Incrementor {
    fn increment(&mut self) -> PodId {
        let id = self.i;
        self.i = self.i.checked_add(1).expect("we ran out of ids");
        id
    }
}
/*
impl Actor for Incrementor {
    type Context = SyncContext<Self>;
}
impl Handler<GetId> for Incrementor {
    type Result = MessageResult<GetId>;
    fn handle(&mut self, _incoming: GetId, _ctx: &mut Self::Context) -> Self::Result {
        MessageResult(NewId(self.increment()))
    }
}

#[derive(Debug, Clone, Message)]
#[rtype(result = "NewId")]
pub struct GetId;

#[derive(Message)]
pub struct NewId(pub PodId);
*/
